package com.codewithbisky.auth.model;

public interface Role {
    String ARTIST = "ARTIST";
    String LISTENER = "LISTENER";
}
